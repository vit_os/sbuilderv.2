import React from 'react';

import {connect} from 'react-redux';
import './App.css';
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";
import NavigationIcon from "@material-ui/icons/Navigation";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import FormDialog from './components/FormDialog/FormDialog';



const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(0.5)
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    }
}));

function App() {
    const classes = useStyles();
    return (
        <div className="App">

            <div className="tools-space">

                <Button variant="outlined"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    10px
                </Button>
                <Button variant="outlined"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    20px
                </Button>
                <Button variant="outlined"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    30px
                </Button>
                <Button variant="outlined"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    40px
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    &#8658;
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    &#8659;
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    ?
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    текст
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    да
                </Button>
                <Button variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                    нет
                </Button>
                <div className="meta-data">
                    <TextField
                        id="standard-dense"
                        label="Заголовок"
                        className={clsx(classes.textField, classes.dense, classes.margin)}
                        margin="dense"
                    />
                    <TextField
                        id="standard-dense"
                        label="Title"
                        className={clsx(classes.textField, classes.dense, classes.margin)}
                        margin="dense"
                    />
                    <TextField
                        id="standard-dense"
                        label="Description"
                        className={clsx(classes.textField, classes.dense, classes.margin)}
                        margin="dense"
                    />
                    <TextField
                        id="standard-dense"
                        label="url страницы"
                        className={clsx(classes.textField, classes.dense, classes.margin)}
                        margin="dense"
                    />
                    <Button
                        variant="contained"
                        size="small"
                        color="primary"
                        className={classes.margin}>
                        Сохранить
                    </Button>


                </div>
            </div>
            <div className="work-space">
                <FormDialog/>


            </div>
        </div>
    );
}

export default App;
